/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package bttn;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author local
 */

class P28 {
    
    public static int[] x = new int[22];
    public static boolean check = false;
    public static void solve1() {
        int N = 44;
        int test, n;
        int[][][] f = new int[N][1005][2];
        int[] p = new int[N];
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            p[0] = 0; p[1] = 1;
            for (int i = 2; i < N; ++i) {
                p[i] = (p[i-1]*10) % n;
            }
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j <= 1; ++j) {
                    f[0][i][j] = -1;
                }
            }
            f[0][0][0] = 0;
            int len = -1;
            for (int i = 1; i < N; ++i) {
                for (int j = 0; j < n; ++j) {
                    for (int k = 0; k <= 1; ++k) {
                        int tmp = (j - p[i] * k + n) % n;
                        f[i][j][k] = -1;
                        for (int l = 0; l <= 1; ++l) {
                            if (f[i-1][tmp][l] != -1) {
                                f[i][j][k] = l*n + tmp;
                                break;
                            }
                        }
                    }
                }
                if (f[i][0][1] != -1) {
                    len = i;
           
                    break;
                }
            }
          
            int tmp = 0, k = 1;
            for (int i = len; i >= 1; --i) {
                System.out.print(k);
                int sum = f[i][tmp][k];
                tmp = sum % n;
                k = sum / n;
            }
            System.out.println();
        }
    }
    
    public static void calc(int n) {
        long sum = 0;
        boolean ok = false;
        for (int i = 1; i <= 20; ++i) {
            if (x[i] == 1) {
                ok = true;
            }
            sum = (sum*10 + x[i]) % n;
        }
        if (ok && sum == 0) {
            ok = false;
            check = true;
            for (int i = 1; i <= 20; ++i) {
                if (x[i] == 1) {
                    ok = true;
                } 
                if (ok) {
                    System.out.print(x[i]);
                }
            }
            System.out.println("");
        }
    }
    
    public static void Try(int i, int n) {
        if (check) {
            return;
        }
        for (int j = 0; j <= 1; ++j) {
            x[i] = j;
            if (i == 20) {
                calc(n);
            }
            else {
                Try(i+1, n);
            }
        }
    }
    
    public static void solve2() {
        int test, n;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            check = false;
            n = sc.nextInt();
            Try(1, n);
        }
    }
}

class P21 {
    static final int N = 100005;
    static int[] a = new int[N];
//    static int[] l = new int[N];
    static int[] r = new int[N];
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test, n;
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            Stack<Integer> st = new Stack<>();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextInt();
            }
            for (int i = n; i >= 1; --i) {
                r[i] = i + 1;
                while (!st.empty() && a[st.peek()] <= a[i]) {
                    r[i] = Math.max(r[i], r[st.pop()]);
                }
                st.push(i);
            }
            
            for (int i = 1; i <= n; ++i) {
                if (r[i] <= n) {
                    System.out.print(a[r[i]]);
                }
                else {
                    System.out.print(-1);
                }
                
                System.out.print(" ");
            }
            System.out.println("");
        }
        
    }
}

class P22 {
    static final int N = 100005;
    static int[] a = new int[N];
    static int[] l = new int[N];
    static int[] r = new int[N];
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test, n;
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            Stack<Integer> st = new Stack<>();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextInt();
            }
            for (int i = n; i >= 1; --i) {
                r[i] = i + 1;
                while (!st.empty() && a[st.peek()] >= a[i]) {
                    r[i] = Math.max(r[i], r[st.pop()]);
                }
                st.push(i);
            }
            while (!st.empty()) {
                st.pop();
            }
            for (int i = 1; i <= n; ++i) {
                l[i] = i - 1;
                while (!st.empty() && a[st.peek()] >= a[i]) {
                    l[i] = Math.min(l[i], l[st.pop()]);
                }
                st.push(i);
            }
            long ans = 0;
            for (int i = 1; i <= n; ++i) {
                ans = Math.max(ans, (long) a[i]*(r[i]-l[i]-1));
            }
            System.out.println(ans);
        }
        
    }
}

public class BTTN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        P22.solve();
    }
    
}
