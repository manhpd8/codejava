/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package bttn;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
/**
 *
 * @author chien
 */

class P11 {
    static class Cost {
        public int a, b, h;
        Cost(int a, int b) {
            this.a = a;
            this.b = b;  
        }
    }
    
    static final int N = 100005;
    static int n, k;
    static Cost[] c = new Cost[N];
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();
        for (int i = 1; i <= n; ++i) {
            c[i] = new Cost(sc.nextInt(), 0);
        }
        int cnt = 0;
        for (int i = 1; i <= n; ++i) {
            c[i].b = sc.nextInt();
            c[i].h = c[i].a - c[i].b;
            if (c[i].h <= 0) {
                ++cnt;
            }
        }
        Arrays.sort(c, 1, n+1, new Comparator<Cost>() {
            public int compare(Cost x, Cost y) {
                return x.h - y.h;
            }
        });
        long ans = 0;
        k = Math.max(cnt, k);
        for (int i = 1; i <= k; ++i) {
            ans += c[i].a;
        }
        for (int i = k+1; i <= n; ++i) {
            ans += c[i].b;
        }
        System.out.println(ans);
    }   
}

class P30 {
    static final int N = 10000007;
    static int[] f = new int[N];
    public static void solve() {
        int test, n;
        Scanner sc = new Scanner(System.in);
        f[1] = 0;
        for (int i = 2; i < N; ++i) {
            f[i] = 123456789;
        }
        for (int i = 1; i < N - 1; ++i) {
            f[i+1] = Math.min(f[i+1], f[i] + 1);
            for (int j = 2; j <= i && i*j < N; ++j) {
                f[i*j] = Math.min(f[i*j], f[i] + 1);
            }
        }
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            System.out.println(f[n]);
        }
    }
}

class P29 {
    static final int N = 20005;
    public static int bfs(int s, int t) {
        int d[] = new int[N];
        for (int i = 1; i < N; ++i) {
            d[i] = 0;
        }
        d[s] = 1;
        Queue<Integer> qu = new LinkedList<>();
        qu.add(s);
        while (qu.size() > 0) {
            int u = qu.poll();
            int v1 = u - 1;
            int v2 = u*2;
            if (v1 >= 1 && v1 < N && d[v1] == 0) {
                d[v1] = d[u] + 1;
                qu.add(v1);
            }
            if (v2 >= 1 && v2 < N && d[v2] == 0) {
                d[v2] = d[u] + 1;
                qu.add(v2);
            }
        }
        return d[t] - 1;
    }
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int i = 1; i <= test; ++i) {
            int s = sc.nextInt();
            int t = sc.nextInt();
            System.out.println(bfs(s, t));
        }
    }
}


class P27 {
    static final int N = 30;
    static long[] f = new long[N];
    public static long calc(String s) {
        int n = s.length();
        long ans = 0;
        for (int i = 1; i < n; ++i) {
            ans = ans + f[i-1];
        }
        if (s.charAt(0) >= '2') {
            ans += f[n-1];
            return ans;
        }
        
        for (int i = 2; i <= n; ++i) {
            if (s.charAt(i-1) >= '2') {
                ans += f[n-i+1];
                break;
            }
            if (s.charAt(i-1) >= '1') {
                ans += f[n-i];
            }
            if (i == n) {
                ++ans;
            }
        }
        return ans;
    }
    
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        f[0] = 1;
        for (int i = 1; i < N; ++i) {
            f[i] = f[i-1]*2;
        }
        for (int i = 1; i <= test; ++i) {
            String s = sc.next();
            System.out.println(calc(s));
        }
    }
}


class P12 {
    public static long power(long n, long k) {
        long ans = 1;
        while (k > 0) {
            if (k % 2 == 1) {
                ans = (ans*n) % 1000000007;
            }
            n = (n*n) % 1000000007;
            k >>= 1;
        }
        return ans;
    }
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            long n = sc.nextLong();
            long k = sc.nextLong();
            System.out.println(power(n, k));
        }
    }
}

class P16 {
    static final long MOD = 1000000007;
    static class Matrix {
        public long[][] a;
        Matrix(int a11, int a12, int a21, int a22) {
            a = new long[3][3];
            a[1][1] = a11;
            a[1][2] = a12;
            a[2][1] = a21;
            a[2][2] = a22;
        }
        
        public Matrix mul(Matrix A) {
            Matrix ans = new Matrix(0, 0, 0, 0);
            for (int i = 1; i <= 2; ++i) {
                for (int j = 1; j <= 2; ++j) {
                    for (int k = 1; k <= 2; ++k) {
                        ans.a[i][j] = (ans.a[i][j] + this.a[i][k] * A.a[k][j]) % MOD;
                    }
                }
            }
            return ans;
        }      
    }
    
    public static Matrix power(Matrix A, int k) {
        Matrix ans = new Matrix(1, 0, 0, 1);
        while (k > 0) {
            if (k % 2 == 1) {
                ans = ans.mul(A);
            }
            A = A.mul(A);
            k >>= 1;
        }
        return ans;
    }
    
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            Matrix A = new Matrix(0, 1, 0, 0);
            Matrix I = new Matrix(0, 1, 1, 1);
            System.out.println(A.mul(power(I, n)).a[1][1]);
        }
    }
}

class P17 {
    static final long MOD = 1000000007;
    static class Matrix {
        public int n;
        public long[][] a;
        Matrix(int n) {
            this.n = n;
            this.a = new long[n+1][n+1];
            for (int i = 1; i <= n; ++i) { 
                for (int j = 1; j <= n; ++j) {
                    this.a[i][j] = 0;
                }
            }
        }
        
        public Matrix mul(Matrix A) {
            Matrix ans = new Matrix(this.n);
            for (int i = 1; i <= this.n; ++i) {
                for (int j = 1; j <= this.n; ++j) {
                    for (int k = 1; k <= this.n; ++k) {
                        ans.a[i][j] = (ans.a[i][j] + this.a[i][k] * A.a[k][j]) % MOD;
                    }
                }
            }
            return ans;
        }    
        
        public void show() {
            for (int i = 1; i <= n; ++i) { 
                for (int j = 1; j <= n; ++j) {
                    System.out.print(this.a[i][j] + " ");
                }
                System.out.println("");
            }
        }
        
    }
    
    public static Matrix power(Matrix A, int k) {
        Matrix ans = new Matrix(A.n);
        for (int i = 1; i <= A.n; ++i) {
            ans.a[i][i] = 1;
        }
        while (k > 0) {
            if (k % 2 == 1) {
                ans = ans.mul(A);
            }
            A = A.mul(A);
            k >>= 1;
        }
        return ans;
    }
    
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            Matrix A = new Matrix(n);
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= n; ++j) {
                    A.a[i][j] = sc.nextInt();
                }
            }
            power(A, k).show();
        }
    }
}
   

class P14 {
    public static long power(long n, long k) {
        long ans = 1;
        while (k > 0) {
            if (k % 2 == 1) {
                ans = (ans*n) % 123456789;
            }
            n = (n*n) % 123456789;
            k >>= 1;
        }
        return ans;
    }
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            long n = sc.nextLong();
            System.out.println(power(2, n-1));
        }
    }
}

class P1234 {
    public static final int N = 105;
    public static int n;
    public static int[] a = new int[N];
    
    public static void show(int t, int m) {
        System.out.print("Buoc " + t + ": ");
        for (int i = 1; i <= m; ++i) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }
    
    public static void solveP1() {
        for (int i = 1; i < n; ++i) {
            for (int j = i+1; j <= n; ++j) {
                if(a[j] < a[i]) {
                    int tmp = a[j];
                    a[j] = a[i];
                    a[i] = tmp;
                }
            }
            show(i, n);
        }
    }
    
    public static void solveP2() {
        for (int i = 1; i < n; ++i) {
            int pos = i;
            for (int j = i+1; j <= n; ++j) {
                if (a[j] < a[pos]) {
                    pos = j;
                }
            }
            int tmp = a[i];
            a[i] = a[pos];
            a[pos] = tmp;
            show(i, n);
        }
    }
    
    public static void solveP3() {
        for (int i = 1; i <= n; ++i) {
            Arrays.sort(a, 1, i+1);
            show(i-1, i);
        }
    }
     
    public static void solveP4() {
        for (int i = 1; i <= n; ++i) {
            boolean ok = false;
            for (int j = 1; j < n; ++j) {
                if (a[j] > a[j+1]) {
                    int tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                    ok = true;
                }
            }
            if (!ok) {
                break;
            }
            show(i, n);
        }
    }
    
    public static void solve(int type) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        for (int i = 1; i <= n; ++i) {
            a[i] = sc.nextInt();
        }
        if (type == 1) {
            solveP1();
        }
        else if (type == 2) {
            solveP2();
        }
        else if (type == 3) {
            solveP3();
        }
        else if (type == 4) {
            solveP4();
        }
    }
}

class P56 {
    public static final int N = 100005;
    public static int n;
    public static long[] a = new long[N];
    public static void solve(int type) {
        int test = 1;
        Scanner sc = new Scanner(System.in);
        if (type == 5) {
            test = sc.nextInt();
        }
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextLong();
            }
            Arrays.sort(a, 1, n+1);
            for (int i = 1; i <= n; ++i) {
                System.out.print(a[i] + " ");
            }
            System.out.println("");
        }
    }
}

class P7 {
    public static final int N = 100005;
    public static int n;
    public static long[] a = new long[N];
    public static long[] b = new long[N];
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextLong();
                b[i] = a[i];
            }
            int cnt = 0;
            Arrays.sort(b, 1, n+1);
            for (int i = 1; i <= n; ++i) {
                if (a[i] == b[i]) {
                    ++cnt;
                }
                else {
                    break;
                }
            }
            for (int i = n; i >= 1; --i) {
                if (a[i] == b[i]) {
                    ++cnt;
                }
                else {
                    break;
                }
            }
            if (cnt >= n) {
                System.out.println("YES");
            }
            else {
                System.out.println(n - cnt);
            }
        }
    }
}

class P8 {
    public static final int N = 100005;
    public static int n;
    public static long[] a = new long[N];
    public static void solve() {
        int test;
        Scanner sc = new Scanner(System.in);
        test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextLong();
            }
            int cnt = 0;
            long min_ = 1234567890;
            Arrays.sort(a, 1, n+1);
            for (int i = 1; i < n; ++i) {
                if (a[i+1] - a[i] < min_) {
                    min_ = a[i+1] - a[i];
                    cnt = 1;
                }
                else if (a[i+1] - a[i] == min_) {
                    ++cnt;
                }
            }
            System.out.println(min_ + " " + cnt);
        }
    }
}

class P9 {
    public static final int N = 100005;
    public static int n, k;
    public static int[] a = new int[N];
    public static int bsearch(int l, int r, int val) {
        for (int i = 1; i <= 50; ++i) {
            int m = (l+r)/2;
            if (a[m] == val) return m;
            else if (a[m] < val) l = m + 1;
            else r = m-1;
        }
        return -1;
    }
    
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            n = sc.nextInt();
            k = sc.nextInt();
            for (int i = 1; i <= n; ++i) {
                a[i] = sc.nextInt();
            }
            if (bsearch(1, n, k) != -1) {
                System.out.println(bsearch(1, n, k));
            }
            else {
                System.out.println("NO");
            }
        }
    }
}

class P10 {
    public static final int N = 1000005;
    public static int n, q;
    public static int[] a = new int[N];

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        a[0] = 0;
        for (int i = 1; i <= n; ++i) {
            ++a[sc.nextInt()];
        }
        for (int i = 1; i < N; ++i) {
            a[i] += a[i-1];
        }
        q = sc.nextInt();
        for (int i = 1; i <= q; ++i) {
            int x = sc.nextInt();
            if (x >= N) x = N-1;
            System.out.println(a[x]);
        }
    }
}


class P13 {
    public static void solve() {
        long[] f = new long[65];
        f[0] = 0; f[1] = 1;
        for (int i = 1; i <= 50; ++i) {
            f[i] = f[i-1]*2 + 1; 
        }
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            while (true) {
//                System.out.println(n + " " + k + " " + f[n]);
                if (k == f[n-1] + 1) {
                    System.out.println(n);
                    break;
                }
                else if (k <= f[n-1]) {
                    --n;
                }
                else {
                    k -= (f[n-1]+1);
                    --n;
                }
            }
        }
    }
}


class P15 {
    public static void solve() {
        long[] f = new long[100];
        f[1] = 1; f[2] = 1;
        for (int i = 3; i <= 93; ++i) {
            f[i] = f[i-1] + f[i-2]; 
        }
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            while (n > 2) {
//                System.out.println(n + " " + k + " " + f[n]);
                if (k <= f[n-2]) {
                    n -= 2;
                }
                else {
                    k -= f[n-2];
                    --n;
                }
            }
            if (n == 1) {
                System.out.println("A");
            }
            else {
                System.out.println("B");
            }
        }
    }
}

class P18 {
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int[] st = new int[555];
        int n = 0;
        String s;
        int x;
        while(sc.hasNext()) {
            s = sc.next();
            if (s.compareTo("push") == 0) {
                x = sc.nextInt();
                st[n++] = x;
            }
            else if (s.compareTo("show") == 0) {
                if (n == 0) {
                    System.out.println("empty");
                    continue;
                }
                for (int i = 0; i < n; ++i) {
                    System.out.print(st[i] + " ");
                }
                System.out.println("");
            }
            else {
                --n;
            }
        }
    }
}

class P23 {
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            Queue<Integer> qu = new LinkedList<Integer>();
            for (int i = 1; i <= n; ++i) {
                int type = sc.nextInt();
                switch(type) {
                    case 1:
                        System.out.println(qu.size());
                        break;
                    case 2:
                        if (qu.size() == 0) {
                            System.out.println("YES");
                        }
                        else {
                            System.out.println("NO");
                        }
                        break;
                    case 3:
                        int x = sc.nextInt();
                        qu.add(x);
                        break;
                    case 4:
                        if (qu.size() > 0) {
                            qu.poll();
                        }
                        break;

                    case 5:
                        if (qu.size() > 0) {
                            System.out.println(qu.toArray()[0]);
                        }
                        else {
                            System.out.println("-1");
                        }
                        break;
                    case 6:
                        if (qu.size() > 0) {
                            System.out.println(qu.toArray()[qu.size() - 1]);
                        }
                        else {
                            System.out.println("-1");
                        }
                        break;
                }
            }
        }
    }
}

class P25 {
    static class Rect {
        public int[] a = new int[7];

        public Rect() {
        }
        
        public int encode() {
            int ans = 0;
            for (int i = 1; i <= 6; ++i) {
                ans = ans * 7 + this.a[i];
            }
            return ans;
        }  
    }
    
    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int[] d = new int[10000007];
        Rect s = new Rect();
        Rect t = new Rect();
        for (int i = 1; i <= 6; ++i) {
            s.a[i] = sc.nextInt();
        }
        for (int i = 1; i <= 6; ++i) {
            t.a[i] = sc.nextInt();
        }
        Queue<Rect> qu = new LinkedList<>();
        qu.add(s);
        d[s.encode()] = 1;
        while(!qu.isEmpty()) {
            Rect u = qu.poll();
            Rect v1 = new Rect();
            Rect v2 = new Rect();
            v1.a[1] = u.a[4];
            v1.a[2] = u.a[1];
            v1.a[3] = u.a[3];
            v1.a[4] = u.a[5];
            v1.a[5] = u.a[2];
            v1.a[6] = u.a[6];
            v2.a[1] = u.a[1];
            v2.a[2] = u.a[5];
            v2.a[3] = u.a[2];
            v2.a[4] = u.a[4];
            v2.a[5] = u.a[6];
            v2.a[6] = u.a[3];
            if (d[v1.encode()] == 0) {
                d[v1.encode()] = d[u.encode()] + 1;
                qu.add(v1);
            }
            if (d[v2.encode()] == 0) {
                d[v2.encode()] = d[u.encode()] + 1;
                qu.add(v2);
            }
        }
        System.out.println(d[t.encode()] - 1);
    }
}

public class BTTN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        P25.solve();
    }
    
}
