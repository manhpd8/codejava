/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test1_L2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai11 {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        int n = Integer.parseInt(inp.nextLine());
        ArrayList<String> list = new ArrayList<>();
        int[] dem = new int[100000];
        while(n>0){
            String s = inp.nextLine();
            s = chuanHoa(s);
            s = tenEmail(s);
            if(list.contains(s)){
                dem[list.indexOf(s)] ++;
                System.out.println(s+dem[list.indexOf(s)]);
            } else{
                list.add(s);
                dem[list.indexOf(s)] ++;
                System.out.println(s);
            }
            n--;
        }
    }
    public static String chuanHoa(String s){
        s = s.replaceAll("\\s+", " ");
        s = s.trim();
        s = s.toLowerCase();
        return s;
    }
    
    public static String tenRieng(String s){
        String re = "";
        String[] temp = s.split("");
        for (String temp1 : temp) {
            re += String.valueOf(temp1.charAt(0)).toUpperCase() + temp1.substring(1);
        }
        return re;
    }
    
    public static String tenEmail(String s){
        String re = "";
        String[] temp = s.split(" ");
        for(int i=1;i<temp.length;i++){
            re += temp[i].charAt(0);
        }
        re = temp[temp.length-1] + re;
        return re;
    }
}
