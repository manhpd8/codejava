/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test1_L2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        while(n>0){
            String s = in.nextLine();
            s = chuanHoa(s);
            s = tenRieng(s);
            System.out.println(s);
            n--;
        }
    }
    
    public static String chuanHoa(String s){
        s = s.replaceAll("\\s+", " ");
        s = s.trim();
        s = s.toLowerCase();
        return s;
    }
    public static String tenRieng(String s){
        String[] temp = s.split(" ");
        String re = "";
        for(String temp1 : temp){
            re += String.valueOf(temp1.charAt(0)).toUpperCase() + temp1.substring(1)+ " ";
        }
        re = re.trim();
        return re;
    }
}
