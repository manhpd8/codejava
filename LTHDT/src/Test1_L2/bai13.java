/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test1_L2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        while(t>0){
            int n = in.nextInt();
            int k = in.nextInt();
            for(int i=1;i<= Math.pow(2, n)-1;i++){
                String s = Integer.toBinaryString(i);
                while(s.length() < n){
                    s = "0"+s;
                }
                int dem =0;
                for(int j=0;j<s.length();j++){
                    if(s.charAt(j)=='1'){
                        dem++;
                    }
                }
                if(dem == k){
                    System.out.println(s);
                }
            }
            t--;
        }
    }
}
