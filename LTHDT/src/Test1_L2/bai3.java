/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test1_L2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai3 {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        int t = inp.nextInt();
        int t1=1;
        while(t>0){
            int n = inp.nextInt();
            int[] a = new int[n+1];
            int[] b = new int[n+1];
            for(int i=1;i<=n;i++){
                a[i] = inp.nextInt();
                b[i] = 1;
            }
            int mmax = 1;
            for(int i=2;i<=n;i++){
                if(a[i]>a[i-1]){
                    b[i] = b[i-1]+1;
                    mmax = Math.max(mmax, b[i]);
                }
            }
            System.out.println("Test "+t1+":");
            t1++;
            System.out.println(mmax);
            for(int i=1;i<=n;i++){
                if(b[i]==mmax){
                    for(int j=mmax-1;j>=0;j--){
                        System.out.print(a[i-j]+" ");
                    }
                    System.out.println("");
                }
            }
            t--;
        }
    }
}
