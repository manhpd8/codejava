/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test1_L2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai7 {
    public static void main(String[] args) {
        Scanner inp =new Scanner(System.in);
        int t = inp.nextInt();
        while(t>0){
            int n= inp.nextInt();
            int[][] a = new int[n+1][n+1];
            int dem = n*n;
            int tr = 1;
            int ph = n;
            while(tr<=ph){
                for(int i=tr;i<=ph;i++){
                    a[tr][i] = dem;
                    dem--;
                }
                
                for(int i=tr+1;i<=ph;i++){
                    a[i][ph] = dem;
                    dem--;
                }
                
                for(int i=ph-1;i>=tr;i--){
                    a[ph][i] = dem;
                    dem--;
                }
                
                for(int i=ph-1;i>=tr+1;i--){
                    a[i][tr] = dem;
                    dem--;
                }
                tr++;
                ph--;
                
            }
            show(a, n);
            t--;
        }
    }
    public static void show(int[][] a, int n){
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                System.out.print(a[i][j]+ " ");
            }
            System.out.println("");
        }
    }
}
