/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai12_luythua {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te = in.nextInt();
        while(te>0){
            long n,k;
            n = in.nextInt();
            k = in.nextInt();
            int mod = 1000000007;
            long re = 1;
            while(k>0){
                if(k%2>0){
                    // nhan o vi tri 1
                    re = re*n%mod;
                }
                n = n*n%mod;
                k/=2;
            }
            System.out.println(re);
            te--;
        }
    }
}
