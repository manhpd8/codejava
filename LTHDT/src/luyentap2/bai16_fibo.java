/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai16_fibo {
    static long mod = 1000000007;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int n;
        while(t>0){
            long[][] a = new long[2][2];
            a[0][0] = 0;
            a[0][1] = 1;
            a[1][0] = 1;
            a[1][1] = 1;
            n = in.nextInt();
            if(n<=2){
                System.out.println("1");
            } else{
                a= luyt(a,n-1);
                System.out.println(a[1][1]);
            }
            
            t--;
        }
    }
    
    static long[][] luyt(long[][] a, int n){
        if(n==1) return a;
        if(n%2==0){
            a = nhan(a,a);
            return luyt(a,n/2);
        } else{
            return nhan(a, luyt(a,n-1));
        }
    }
    
    static long[][] nhan(long[][] a,long[][] b){
        long[][] c = new long[2][2];
        c[0][0] = a[0][0]*b[0][0]+ a[0][1]*b[1][0];
        c[0][1] = a[0][0]*b[0][1]+ a[0][1]*b[1][1];
        c[1][0] = a[1][0]*b[0][0]+ a[1][1]*b[1][0];
        c[1][1] = a[1][0]*b[0][1]+ a[1][1]*b[1][1];
        c[0][0] %= mod;
        c[0][1] %= mod;
        c[1][0] %= mod;
        c[1][1] %= mod;
        return c;
    }
}
