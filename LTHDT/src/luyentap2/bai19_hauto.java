/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author LuuLy
 */
public class bai19_hauto {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        while(n>0){
            String s = in.nextLine();
            s = s.replaceAll("\\s+", "");
            Stack st = new Stack();
            for(int i=0;i<s.length();i++){
                if(s.charAt(i)>='a' && s.charAt(i)<='z'){
                    System.out.print(s.charAt(i));
                } else{
                    if(s.charAt(i)=='('){
                        st.add('(');
                    }
                    if(s.charAt(i)==')'){
                        while(!st.empty()){
                            char a = (char) st.peek();
                            if(a==')'){
                                st.pop();
                                break;
                            } else{
                                System.out.print(st.pop());
                            }
                        }
                    }
                    if(istoantu(s.charAt(i))){
                        if(istoantu((char) st.peek())){
                            if(uutien((char) st.peek())>= uutien(s.charAt(i))){
                                System.out.print(st.pop());
                                st.push(s.charAt(i));
                            }
                        } else{
                            System.out.print(s.charAt(i));
                        }
                    }
                }
            }
            System.out.println("");
            n--;
        }
    }
    
    static int uutien(char a){
        if(a=='*' || a =='/'){
            return 2;
        }
        if(a=='+' || a=='-'){
            return 1;
        }
        return 0;
    }
    
    static boolean istoantu(char a){
        if(a=='+' || a=='-' || a=='*' || a=='/'){
            return true;
        }
        return false;
    }
}
