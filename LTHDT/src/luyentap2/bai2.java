/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, dem;
        int[] a = new int[101];
        String da = "";
        dem = 1;
        n = in.nextInt();
        for (int i=1; i<=n; i++) {
            a[i] = in.nextInt();
        }
        for (int i=1; i<n; i++) {
            int mmin = getMin(i,n,a);
            int temp = a[i];
            if (mmin != i) {
                a[i] = a[mmin];
                a[mmin] = temp;
            }
            String sShow = show(n,a);
            System.out.println("Buoc "+dem+": "+sShow);
            dem++;
        }
    }
    
    public static String show(int n, int[] a) {
        String da = "";
        for (int i=1; i<=n; i++) {
            da+= a[i]+" ";
        }
        da = da.trim();
        return da;
    }
    
    public static int getMin(int start, int n, int[] a) {
        int mmin = start;
        for (int i = start+1; i<=n; i++) {
            if (a[i]<a[mmin]) {
                mmin = i;
            }
        }
        return mmin;
    }
}
