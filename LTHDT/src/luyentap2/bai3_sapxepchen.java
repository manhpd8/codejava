/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai3_sapxepchen {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        int[] a = new int[101];
        n = in.nextInt();
        for (int i=0; i<n; i++) {
            a[i] = in.nextInt();
        }
        int dem = 0;
        for(int i=0;i<n;i++){
            int[] aClone = Arrays.copyOfRange(a, 0, i+1);
            Arrays.sort(aClone);
            System.out.println("Buoc "+dem+": "+show(aClone));
            dem++;
        }
    }
    static String show(int[] a){
        String da = "";
        for(int i=0;i<a.length;i++){
            da+=a[i]+" ";
        }
        return da.trim();
    }
}
