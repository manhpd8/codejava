/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai4_sapxepnoibot {
    boolean ok = false;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        int[] a = new int[101];
        n = in.nextInt();
        for (int i=0; i<n; i++) {
            a[i] = in.nextInt();
        }
        int dem = 1;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(a[j]>a[j+1] && j+1<n){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1]= temp;
                }
            }
            System.out.println("Buoc "+dem+": "+show(a,n));
            dem++;
            if(ktra(a,n)){
                return;
            }
        }
    }
    static String show(int[] a,int n){
        String da = "";
        for(int i=0;i<n;i++){
            da+=a[i]+" ";
        }
        return da.trim();
    }
    
    static boolean ktra(int[] a,int n){
        for(int i=0;i<n-1;i++){
            if(a[i]>a[i+1]) {
                return false;
            }
        }
        return true;
    }
}
