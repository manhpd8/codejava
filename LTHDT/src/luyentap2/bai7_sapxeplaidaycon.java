/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai7_sapxeplaidaycon {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        while(t>0){
            int n = in.nextInt();
            boolean[] a = new boolean[1000006];
            int[] origin = new int[n];
            int[] dem = new int[1000006];
            int iin ;
            for(int i=0;i<n;i++){
                iin = in.nextInt();
                a[iin] = true;
                origin[i] = iin;
                dem[iin]++;
            }
            int iauto=0;
            int[] asort = new int[n];
            for(int i=0;i<=1000005;i++){
                if(a[i]){
                    for(int j=1;j<=dem[i];j++){
                        asort[iauto] = i;
                        iauto++;
                    }
                }
            }
            int i=0;
            int j=n-1;
            while(i<=j){
                if(origin[i]!=asort[i]){
                    break;
                } else{
                    i++;
                }
            }
            if(i>=n){
                System.out.println("YES");
            } else{
                while(j>i){
                    if(origin[j]!=asort[j]){
                        break;
                    } else{
                        j--;
                    }
                }
                System.out.println(j-i+1);
            }
            t--;
        }
    }
}
