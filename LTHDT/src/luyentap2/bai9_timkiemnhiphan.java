/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap2;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai9_timkiemnhiphan {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        while(n1>0){
            int n = in.nextInt();
            int k = in.nextInt();
            int[] a = new int[n+1];
            for(int i=1;i<=n;i++){
                a[i] = in.nextInt();
            }
            String da = binarySearch(0, n, k, a);
            System.out.println(da);
            n1--;
        }
    }
    
    static String binarySearch(int l, int r, int x, int[] a) {
        while(l<=r){
            int mid = (l+r)/2;
            if(a[mid]==x){
                return mid+"";
            }
            if(a[mid]<x){
                l=mid+1;
            }
            if(a[mid]>x){
                r=mid-1;
            }
        }
        return "NO";
    }
}
