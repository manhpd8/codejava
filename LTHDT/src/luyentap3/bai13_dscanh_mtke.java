/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai13_dscanh_mtke {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = Integer.parseInt(in.nextLine());
        int[][] a = new int[n+1][n+1];
        String sin;
        for(int i =1;i<=n;i++){
            sin = in.nextLine();
            String[] dinhs = sin.split(" ");
            for (String dinh : dinhs) {
                a[i][Integer.parseInt(dinh)] = 1;
                a[Integer.parseInt(dinh)][i] = 1;
            }
        }
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println("");
        }
    }
}
