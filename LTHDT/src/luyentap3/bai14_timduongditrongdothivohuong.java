/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author LuuLy
 */
public class bai14_timduongditrongdothivohuong {
    static boolean ok =false;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te = in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            boolean[][] a = new boolean[n+1][n+1];
            int x,y;
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            int q= in.nextInt();
            for(int i=1;i<=q;i++){
                x = in.nextInt();
                y = in.nextInt();
                ok = false;
                boolean[] dau = new boolean[n+1];
                dau[x] = true;
                dfs(a,n,x,y,dau);
                if(ok){
                    System.out.println("YES");
                } else{
                    System.out.println("NO");
                }
            }
            te--;
        }
    }
    
    @SuppressWarnings("empty-statement")
    static void dfs( boolean[][] a, int n, int dd, int dc, boolean[] dau){
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i] == false)){
                if(i==dc){
                    ok = true;
                    return;
                } else{
                    dau[i] = true;
                    dfs(a,n,i,dc,dau);
                }
            };
        }
    }
}
