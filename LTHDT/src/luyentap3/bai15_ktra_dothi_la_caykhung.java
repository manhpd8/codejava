package luyentap3;

import java.util.LinkedList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lamvi
 */
public class bai15_ktra_dothi_la_caykhung {
    static int dem;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te= in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = n-1;
            int u = 1;
            int x,y;
            boolean[][] a = new boolean[1003][1003];
            boolean[] dau = new boolean[n+1];
            LinkedList<Integer> ll = new LinkedList<>();
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            dem =0;
            dau[u] = true;
            ll.add(u);
            Bfs(a,n,ll,dau);
            if(dem==n){
                System.out.println("NO");
            } else{
                System.out.println("YES");
            }
            te--;
        }
    }
    static void Bfs(boolean[][] a,int n,LinkedList<Integer> ll, boolean[] dau){
        int dd = ll.getFirst();
        ll.pop();
        dau[dd] = true;
        dem++;
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i]==false)){
                ll.add(i);
                dau[i] = true;
            }
        }
        if(!ll.isEmpty()){
            Bfs(a,n,ll,dau);
        }
    }
}
