/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai17_bfs {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te= in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            int u = in.nextInt();
            int x,y;
            boolean[][] a = new boolean[1003][1003];
            boolean[] dau = new boolean[n+1];
            LinkedList<Integer> ll = new LinkedList<>();
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            dau[u] = true;
            ll.add(u);
            Bfs(a,n,ll,dau);
            System.out.println("");
            te--;
        }
    }
    static void Bfs(boolean[][] a,int n,LinkedList<Integer> ll, boolean[] dau){
        int dd = ll.getFirst();
        ll.pop();
        dau[dd] = true;
        System.out.print(dd+" ");
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i]==false)){
                ll.add(i);
                dau[i] = true;
            }
        }
        if(!ll.isEmpty()){
            Bfs(a,n,ll,dau);
        }
    }
}
