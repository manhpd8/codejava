/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai18_dfs {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te= in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            int u = in.nextInt();
            int x,y;
            boolean[][] a = new boolean[1003][1003];
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            boolean[] dau = new boolean[n+1];
            dfs(a,n,u,dau);
            System.out.println("");
            te--;
        }
    }
    static void dfs(boolean[][] a,int n,int dd,boolean[] dau){
        System.out.print(dd+" ");
        dau[dd] = true;
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i]==false)){
                dfs(a,n,i,dau);
            }
        }
    }
}
