/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai20_dfs_lienthong {
    static boolean[] dau;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te = in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            int x,y;
            boolean[][] a = new boolean[n+1][n+1];
            dau = new boolean[n+1];
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            for(int i=1;i<=n;i++){
                if(!dau[i]){
                    Dfs(a,n,i);
                    System.out.println("");
                }
            }
            te--;
        }
    }
    static void Dfs(boolean[][] a,int n,int dd){
        System.out.print(dd+" ");
        dau[dd] = true;
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i] == false)){
                Dfs(a,n,i);
            }
        }
    }
}
