/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai21_bfs_timduongdi {
    static int[] ql;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te= in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            int u = in.nextInt();
            int v = in.nextInt();
            int x,y;
            boolean[][] a = new boolean[1003][1003];
            boolean[] dau = new boolean[n+1];
            LinkedList<Integer> ll = new LinkedList<>();
            ql = new int[n+1];
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            if(u==v){
                System.out.println(u);
            } else {
                dau[u] = true;
                ll.add(u);
                Bfs(a,n,ll,dau,v);
                if(ql[v] == 0){
                    System.out.println("-1");
                } else{
                    System.out.println(demd(u,v));
                }
            }
            te--;
        }
    }

    static void Bfs(boolean[][] a,int n,LinkedList<Integer> ll, boolean[] dau,int dc){
        int dd = ll.getFirst();
        ll.pop();
        dau[dd] = true;
        if(dd == dc){
            return;
        }
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i]==false)){
                ll.add(i);
                ql[i] = dd;
                dau[i] = true;
            }
        }
        if(!ll.isEmpty()){
            Bfs(a,n,ll,dau,dc);
        }
    }
    
    static int demd(int u, int v){
        if(ql[v] == 0) {
            return -1;
        }
        int dem = 0;
        while(v!=u){
            dem++;
            v = ql[v];
        }
        return dem;
    }
}
