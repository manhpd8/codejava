/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai22_dfs_dembuocdi {
    static int[] ql;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te= in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int m = in.nextInt();
            int u = in.nextInt();
            int v = in.nextInt();
            int x,y;
            boolean[][] a = new boolean[1003][1003];
            for(int i=1;i<=m;i++){
                x = in.nextInt();
                y = in.nextInt();
                a[x][y] = true;
                a[y][x] = true;
            }
            boolean[] dau = new boolean[n+1];
            ql = new int[n+1];
            if(u==v){
                System.out.println(u);
            } else {
                dau[u] = true;
                dfs(a,n,u,dau);
                if(ql[v] == 0){
                    System.out.println("-1");
                } else{
                    System.out.println(demd(u,v));
                }
            }
            te--;
        }
    }
    static void dfs(boolean[][] a,int n,int dd,boolean[] dau){
        dau[dd] = true;
        for(int i=1;i<=n;i++){
            if(a[dd][i] && (dau[i]==false)){
                ql[i] = dd;
                dfs(a,n,i,dau);
                ql[i] = 0;
            }
        }
    }
    
    static int demd(int u, int v){
        if(ql[v] == 0) {
            return -1;
        }
        int dem = 0;
        while(v!=u){
            dem++;
            v = ql[v];
        }
        return dem;
    }
}
