/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai3_daycondainhat {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n+1];
        int[] da = new int[n+1];
        for(int i=0;i<n;i++){
            a[i] = in.nextInt();
        }
        da[0] = 1;
        int damax = 1;
        for(int i=1;i<n;i++){
            int dmax = 0;
            for(int j=0;j<i;j++){
                if(a[j]<a[i] && da[j]>dmax){
                    dmax = da[j];
                }
            }
            da[i] = dmax+1;
            damax = Math.max(damax, da[i]);
        }
        System.out.println(damax);
    }
}
