/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai4_day_con_tong_s {
    /*
    5 6
    1 2 4 3 5
    */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int s = in.nextInt();
        int[] a = new int[n+1];
        boolean[][] da = new boolean[n+1][s+1];
        for(int i=1;i<=n;i++){
            a[i] = in.nextInt();
            da[i][a[i]] = true;
        }
        for(int i=1;i<=n;i++){
            for(int t=s;t>=a[i];t--){
                if(da[i-1][t] || da[i-1][t-a[i]]) {
                    da[i][t] = true;
                }
            }
        }
        if(da[n][s]){
            System.out.println("YES");
        } else{
            System.out.println("NO");
        }
    }
    
}
