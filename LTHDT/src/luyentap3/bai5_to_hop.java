/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luyentap3;

import java.util.Scanner;

/**
 *
 * @author lamvi
 */
public class bai5_to_hop {
    static long mod = 1000000007;
    static long gt(int n){
        long da= 1;
        for(int i=2;i<=n;i++){
            da = da*i%mod;
        }
        return da;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int te = in.nextInt();
        while(te>0){
            int n = in.nextInt();
            int k = in.nextInt();
            double da = 1;
            for(int i=1;i<=n-k;i++){
                da = da*(k+i)/i;
                da %=mod;
            }
            System.out.println((long)da);
            te--;
        }
    }
    
    
}
