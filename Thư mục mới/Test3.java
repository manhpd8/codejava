/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package test3;

/**
 *
 * @author local
 */
import java.math.BigInteger;
import java.util.*;

class P1 {

    public static void solve() {
        int c, n;
        int[] w = new int[105];
        Scanner sc = new Scanner(System.in);
        c = sc.nextInt();
        n = sc.nextInt();
        for (int i = 1; i <= n; ++i) {
            w[i] = sc.nextInt();
        }
        int[][] f = new int[c + 5][n + 5];
        for (int i = 0; i <= c; ++i) {
            for (int j = 1; j <= n; ++j) {
                f[i][j] = f[i][j - 1];
                if (i >= w[j]) {
                    f[i][j] = Math.max(f[i][j], f[i - w[j]][j - 1] + w[j]);
                }
            }
        }
        System.out.println(f[c][n]);
    }
}

class P2 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        String A = sc.next();
        String B = sc.next();
        int lenA = A.length();
        int lenB = B.length();
        int[][] f = new int[lenA + 5][lenB + 5];
        for (int i = 1; i <= lenA; ++i) {
            for (int j = 1; j <= lenB; ++j) {
                f[i][j] = Math.max(f[i - 1][j], f[i][j - 1]);
                if (A.charAt(i - 1) == B.charAt(j - 1)) {
                    f[i][j] = Math.max(f[i][j], f[i - 1][j - 1] + 1);
                }
            }
        }
        System.out.println(f[lenA][lenB]);
    }
}

class P3 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n + 5];
        int[] f = new int[n + 5];
        int ans = 0;
        for (int i = 1; i <= n; ++i) {
            a[i] = sc.nextInt();
            f[i] = 1;
            for (int j = 1; j < i; ++j) {
                if (a[j] < a[i]) {
                    f[i] = Math.max(f[i], f[j] + 1);
                }
            }
            ans = Math.max(ans, f[i]);
        }
        System.out.println(ans);
    }
}

class P4 {

    public static void solve() {
        int s, n;
        int[] a = new int[105];
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        s = sc.nextInt();
        for (int i = 1; i <= n; ++i) {
            a[i] = sc.nextInt();
        }
        int[][] f = new int[s + 5][n + 5];
        f[0][0] = 1;
        for (int i = 0; i <= s; ++i) {
            for (int j = 1; j <= n; ++j) {
                f[i][j] = f[i][j - 1];
                if (i >= a[j]) {
                    f[i][j] = Math.max(f[i][j], f[i - a[j]][j - 1]);
                }
            }
        }
        if (f[s][n] > 0) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}

class P5 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            BigInteger ans = BigInteger.ONE;
            for (int i = k + 1; i <= n; ++i) {
                ans = ans.multiply(BigInteger.valueOf(i));
            }
            for (int i = 1; i <= n - k; ++i) {
                ans = ans.divide(BigInteger.valueOf(i));
            }
            ans = ans.mod(BigInteger.valueOf(1000000007));
            System.out.println(ans);
        }
    }
}

class P6 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            String S = sc.next();
            int lenS = S.length();
            int[][] f = new int[lenS + 5][lenS + 5];
            int ans = 0;
            for (int i = 1; i <= lenS; ++i) {
                for (int j = lenS; j >= i; --j) {
//                    f[i][j] = Math.max(f[i - 1][j], f[i][j + 1]);
                    if (S.charAt(i - 1) == S.charAt(j - 1)) {
                        f[i][j] = Math.max(f[i][j], f[i - 1][j + 1] + 1 + (i == j ? 0 : 1));
                    }
                    if (j - i <= 1) ans = Math.max(ans, f[i][j]);
                }
            }
            System.out.println(ans);
        }
    }
}

class P7 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            int[] f = new int[n+5];
            f[0] = 1;
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= k; ++j) {
                    if (i >= j) {
                        f[i] = (f[i] + f[i-j]) % 1000000007;
                    }
                }
            }
            System.out.println(f[n]);
        }
    }
}

class P8 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            int[][] f = new int[n+5][k+5];
            f[0][0] = 1;
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= k; ++j) {
                    int st = 0;
                    if (i == 1) {
                        st = 1;
                    }
                    for (int b = st; b <= 9; ++b) {
                        if (j >= b) f[i][j] = (f[i][j] + f[i-1][j-b]) % 1000000007;
                    }
                }
            }
            System.out.println(f[n][k]);
        }
    }
}

class P9 {

    public static void solve() {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int t = 1; t <= test; ++t) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] a = new int[n+5][m+5];
            int[][] f = new int[n+5][m+5];
            f[0][0] = 1;
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= m; ++j) {
                    a[i][j] = sc.nextInt();
                }
            }
            f[1][1] = a[1][1];
            for (int i = 1; i <= n; ++i) {
                for (int j = 1; j <= m; ++j) {
                    if (i != 1 || j != 1) {
                        f[i][j] = 123456789;
                    }
                    if (i > 1) {
                        f[i][j] = Math.min(f[i][j], f[i-1][j] + a[i][j]);
                    }
                    if (j > 1) {
                        f[i][j] = Math.min(f[i][j], f[i][j-1] + a[i][j]);
                    }
                    if (i > 1 && j > 1) {
                        f[i][j] = Math.min(f[i][j], f[i-1][j-1] + a[i][j]);
                    }
                }
            }
            System.out.println(f[n][m]);
        }
    }
}


public class Test3 {

    public static void main(String[] args) {
        P7.solve();
    }
}
